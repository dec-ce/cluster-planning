**NSW Department of Education - Cluster Planning**
========

## Version
--------------
1.0.0

## Table of Contents
--------------
  1. [Technology](#technology)
  1. [Useage](#usage)
  1. [Installation Dependancies](#installation-dependancies)
  1. [Development](#development)
  1. [HTML Templating](#html-templating)
  1. [CSS](#css)
     - [Formatting](#formatting)
     - [Comments](#comments)
     - [Naming Convenstion](#naming-convenstion)
     - [Ordering of property declarations](#ordering-of-property-declarations)
     - [Variables](#variables)
     - [Mixins](#mixins)
     - [Nested selectors](#nested-selectors)
  1. [Grid](#framework-grid)
  1. [Testing](#testing)
  1. [Building for Matrix import](#building-for-matrix-import)


## Technology
-----------
**NSW Department of Education - Cluster Planning** has been created to utilise the following plugins:

* [NPM](https://www.npmjs.com/) - Package Manager
* [SASS](http://sass-lang.com/) - CSS preprocessor
* [Gulp](http://gulpjs.com/) - automated JS task runner
* [Pugjs](https://pugjs.org/api/getting-started.html) - Node.js templating language
* [Babel](https://babeljs.io/) - ES6 Javascipt compiler

And contains the following linting, derived from the AirBnB framework:
* ESLint
* SASS linting


## Installation Dependancies
--------------

Install the UI Framework dependencies via
```sh
$ npm install
```
These will be predefined via the package.json file



## Development
--------------

Via the command line, simply run the development gulp task by using:
```sh
$ npm start
```
Browsersync will automatic open a window running a localhost. If not, check the gulp cmd window or you should be able to see the site running at `http://localhost:3000/`
The gulp tasks for development includes a watch and live reload feature.

All source/working files are under the `src` folder



## HTML Templating
--------------

The UI Framework HTML runs on the [pugjs] templating engine and is complied into static html files with the `dist` folder.

The base settings can be found in the `src\assets\pug\core.pug` with any patterns or components found under `src\assets\pug\components` and `src\assets\pug\patterns`.
These files also have corresponding scss files in `src\assets\scss` in a mirrored folder structure.



## CSS
--------------

### Formatting

* Use soft tabs (2 spaces) for indentation
* Prefer dashes over camelCasing in class names.
  - Underscores and PascalCasing are okay if you are using BEM (see [Naming Convenstion](#naming-convenstion) below).
* Do not use ID selectors
* When using multiple selectors in a rule declaration, give each selector its own line.
* Put a space before the opening brace `{` in rule declarations
* In properties, put a space after, but not before, the `:` character.
* Put closing braces `}` of rule declarations on a new line
* Put blank lines between rule declarations

### Comments

* Prefer line comments (`//` in Sass-land) to block comments.
* Prefer comments on their own line. Avoid end-of-line comments.
* Write detailed comments for code that isn't self-documenting:
  - Uses of z-index
  - Compatibility or browser-specific hacks

### Naming Convenstion

We encourage some combination of OOCSS and BEM and ITCSS for these reasons:

  * It helps create clear, strict relationships between CSS and HTML
  * It helps us create reusable, composable components
  * It allows for less nesting and lower specificity
  * It helps in building scalable stylesheets

**OOCSS**, or "Object Oriented CSS", is an approach for writing CSS that encourages you to think about your stylesheets as a collection of "objects": reusable, repeatable snippets that can be used independently throughout a website.

  * Nicole Sullivan's [OOCSS wiki](https://github.com/stubbornella/oocss/wiki)
  * Smashing Magazine's [Introduction to OOCSS](http://www.smashingmagazine.com/2011/12/12/an-introduction-to-object-oriented-css-oocss/)

**BEM**, or "Block-Element-Modifier", is a _naming convention_ for classes in HTML and CSS. It was originally developed by Yandex with large codebases and scalability in mind, and can serve as a solid set of guidelines for implementing OOCSS.

  * CSS Trick's [BEM 101](https://css-tricks.com/bem-101/)
  * Harry Roberts' [introduction to BEM](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

We recommend a variant of BEM with PascalCased "blocks", which works particularly well when combined with components (e.g. React). Underscores and dashes are still used for modifiers and children.

**ITCSS**, or "Inverted Triange CSS", is a scalable, managed, CSS architecture from CSS Wizardry. The most common types of namespace are c-, for Components, o-, for Objects, u-, for Utilities, and is-/has- for States.

  * BEMIT: Taking the BEM Naming Convention a Step Further [BEMIT](http://csswizardry.com/2015/08/bemit-taking-the-bem-naming-convention-a-step-further/)

### Ordering of property declarations

1. Property declarations

    List all standard property declarations, anything that isn't an `@include` or a nested selector.

    ```scss
    .btn-green {
      background: green;
      font-weight: bold;
      // ...
    }
    ```

2. `@include` declarations

    Grouping `@include`s at the end makes it easier to read the entire selector.

    ```scss
    .btn-green {
      background: green;
      font-weight: bold;
      @include transition(background 0.5s ease);
      // ...
    }
    ```

3. Nested selectors

    Nested selectors, _if necessary_, go last, and nothing goes after them. Add whitespace between your rule declarations and nested selectors, as well as between adjacent nested selectors. Apply the same guidelines as above to your nested selectors.

    ```scss
    .btn {
      background: green;
      font-weight: bold;
      @include transition(background 0.5s ease);

      .icon {
        margin-right: 10px;
      }
    }
    ```

### Variables

Prefer dash-cased variable names (e.g. `$my-variable`) over camelCased or snake_cased variable names. It is acceptable to prefix variable names that are intended to be used only within the same file with an underscore (e.g. `$_my-variable`).
Blocks of variables defined within the base/_settings.scss such as font weight can be used via a mapping technique:
```scss
font-weight: map-get($font-weight, 'light');
```

### Mixins

Mixins should be used to DRY up your code, add clarity, or abstract complexity--in much the same way as well-named functions. Mixins that accept no arguments can be useful for this, but note that if you are not compressing your payload (e.g. gzip), this may contribute to unnecessary code duplication in the resulting styles.

### Nested selectors

**Do not nest selectors more than three levels deep!**

```scss
.page-container {
  .content {
    .profile {
      // STOP!
    }
  }
}
```

When selectors become this long, you're likely writing CSS that is:

* Strongly coupled to the HTML (fragile) *-OR-*
* Overly specific (powerful) *-OR-*
* Not reusable


## Framework Grid
--------------
The grid included with the framework runs on the same diminsions and breakpoints as the 12 col grid supplied with bootstrap v4.
This can be customised at any time via the base/_settings.scss file.

The grid has a set of basic utility classes such as .grid__col--25 which is a column of 25% or .grid__col--1 throught to grid__col--12 for each column.
It is best to use the mixin methods to create the grid structure within the scss code to avoid a large amount of classes within the html structure, and easlier to maintain later on.

For a container:
```scss
.container {
  @include make-container();
  @include make-container-max-widths();
}
```

For a row:
```scss
.row {
  @include make-row();
}
```

For columns:
```scss
.column {
  @include make-col();
  @include make-col-span(4);

  @include media('screen', '>sm') { @include make-col-span(12) }
}
```


## Testing
--------------
### Visual Regression:
[Backstop.js](https://garris.github.io/BackstopJS/) - follow the setup instructions at: https://css-tricks.com/automating-css-regression-testing/

### Accessibility:
[Gulp-a11y](https://www.npmjs.com/package/gulp-a11y) - A Gulp plugin for a11y to run accessibility audits on html files



## Building for Matrix import
--------------

When your ready to build out the files, use the following command:
```sh
$ npm run build
```
This includes a find and replace for images and fonts directories to 'mysource_files' and minification
