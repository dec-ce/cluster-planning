$('#searchTypeToggle').customSelect()
$('.customSelect').append('<i class="icon-chevron-down" aria-hidden="true" />')
/** Links **/

class Links {

  constructor() {
    this.smoothScroll()
    $.each($('#main__content').find('a:not([class*="addthis"], [href="#"], .timeline__link)'), this.externalLink())
  }

  smoothScroll() {
    $('a[href*="#"]:not(.skip-link, .tabs__group a, [href="#"])').click((evt) => {
      const link = evt.currentTarget
      if (location.pathname.replace(/^\//, '') === link.pathname.replace(/^\//, '') && location.hostname === link.hostname) {
        let $target = $(evt.currentTarget.hash)
        $target = $target.length ? $target : $(`[name=${link.hash.slice(1)}]`)
        if ($target.length) {
          $('html, body').animate({ scrollTop: $target.offset().top }, 1000, () => { $target.focus() })
          return false
        }
      }
    })
  }

  externalLink() {
    return (_, link) => {
      const $hostname = link.hostname
      if (($hostname !== location.hostname) && ($hostname.indexOf('dec') === -1)) {
        $(link).append('<i class="icon-external-link"><span class="sr-only">external link</span></i>')
        // $(link).addClass('link--external')
      }
    }
  }
}

const links = new Links()

/** Accordion **/

class Accordion {
  constructor() {
    this.config = {
      $allPanels: $('.accordion__title + div'),
      $anchors: $('.accordion__title'),
    }

    this.setUpAccordions(this.config)
  }

  setUpAccordions(config) {
    config.$allPanels.attr({
        tabindex: -1,
        'aria-expanded': false,
        'aria-hidden': true,
      }).hide()

    config.$anchors.attr('aria-expanded',false)

    config.$anchors.click((evt) => {
      const title = evt.currentTarget
      evt.preventDefault()
      evt.stopImmediatePropagation()
      this.triggerAccordion(this.config.$anchors, this.config.$allPanels, title)
    }).keydown((evt) => {
      const keyCode = window.event ? evt.which : evt.keyCode
      if (keyCode === 13) {
        $(evt.currentTarget).click()
        return false
      }
    })
  }

  triggerAccordion(anchors, panels, title) {
    const $title = $(title)

    if ($title.attr('aria-expanded') === 'false') {
      anchors.attr('aria-expanded', false)

      panels.attr({
        tabindex: -1,
        'aria-expanded': false,
        'aria-hidden': true,
      }).slideUp()

      $title.attr('aria-expanded', true)

      $title.next().attr({
        tabindex: 0,
        'aria-expanded': true,
        'aria-hidden': false,
      }).slideDown()
    } else {
      $title.attr('aria-expanded', false)

      $title.next().attr({
        tabindex: -1,
        'aria-expanded': false,
        'aria-hidden': true,
      }).slideUp()
    }

    return false
  }
}

const accordion = new Accordion()


/** Tabs **/

class Tabs {

  constructor() {
    this.config = {
      $container: $('.tabs'),
      anchors: '[role=tab]',
      panels: '[role=tabpanel]',
      active: 'tabs--active',
    }

    this.$container = this.config.$container
    $.each(this.$container, this.setUpTabs(this.config))
  }

  setUpTabs(config) {
    return (_, tabs) => {
      const $tabs = $(tabs)
      const $panels = $tabs.find(config.panels)
      const $anchors = $tabs.find(config.anchors)

      $tabs.addClass(`${config.active} js-tabs`).append('<ul class="tabs__panel-group" />')

      $panels.attr('aria-hidden', true).wrap('<li />')
      $panels.eq(0).attr('aria-hidden', false)
      $panels.parent().appendTo($tabs.find('.tabs__panel-group'))

      $anchors.attr('aria-selected', false)
      $anchors.eq(0).attr('aria-selected', true)

      $.each($anchors, this.setUpAnchors($panels, $anchors))
    }
  }

  setUpAnchors(panels, anchors) {
    return (_, anchor) => {
      $(anchor).click((evt) => {
        evt.preventDefault()
        evt.stopPropagation()

        const $anchor = $(evt.currentTarget)
        const tabPanelCntr = $anchor.attr('aria-controls')
        const $tabPanel = $(`#${tabPanelCntr}`)

        anchors.attr('aria-selected', false)
        $anchor.attr('aria-selected', true)

        panels.attr('aria-hidden', true)
        $tabPanel.attr('aria-hidden', false)
      }).keydown((evt) => {
        const keyCode = window.event ? evt.which : evt.keyCode
        if (keyCode === 13) {
          $(evt.currentTarget).click()
          return false
        }
      })
    }
  }
}

const tabs = new Tabs()


/** Readmore **/

class Readmore {

  constructor() {
    this.config = {
      showChar: 100,
      ellipsesText: '...',
      moreText: 'show more >',
      lessText: 'show less',
      $paragraph: $('.more'),
    }

    $.each(this.config.$paragraph, this.setUpConcat(this.config))
  }

  setUpConcat(config) {
    return (_, paragraph) => {
      const $paragraph = $(paragraph)
      const content = $paragraph.html()
      const showChar = ($paragraph.attr('data-more-length')) ? $paragraph.data('more-length') : config.showChar
      const moreText = ($paragraph.data('more-text-show')) ? $paragraph.data('more-text-show') : config.moreText
      const lessText = ($paragraph.data('more-text-hide')) ? $paragraph.data('more-text-hide') : config.lessText

      if (content.length > showChar) {
        const c = content.substr(0, showChar)
        const h = content.substr(showChar, content.length - showChar)
        const html = `${c}<span class='more__ellipses'>${config.ellipsesText}</span>
                      <span class='more__content'><span>${h}</span>
                      <a href='#' class='more__link'>${moreText}</a></span>`
        $paragraph.html(html)
      }

      this.setUpAnchors($paragraph.find('.more__link'), moreText, lessText)
    }
  }

  setUpAnchors(element, moreText, lessText) {
    $(element).click((evt) => {
      evt.preventDefault()
      evt.stopPropagation()

      const $anchor = $(evt.currentTarget)

      if ($anchor.hasClass('less')) {
        $anchor.removeClass('less').html(moreText)
      } else {
        $anchor.addClass('less').html(lessText)
      }

      $anchor.parent().prev().toggle()
      $anchor.prev().toggle()
    }).keydown((evt) => {
      const keyCode = window.event ? evt.which : evt.keyCode
      if (keyCode === 13) {
        $(evt.currentTarget).click()
        return false
      }
    })
  }
}

const readmore = new Readmore()


/** Timeline **/

class Timeline {

  constructor() {
    this.config = {
      $showHideMore: $('.timeline__items'),
    }

    $.each(this.config.$showHideMore, this.setUpTimeline(this.config))
    this.setUpToggle(this.config.$showHideMore.find('.timeline__toggle'))
  }

  setUpTimeline(config) {
    return (_, timeline) => {
      const $timeline = $(timeline)
      const $times = $timeline.children('.timeline__item:not(.timeline__item--today)')
      const $showNumber = $timeline.data('timeline-show')
      const $showText = $timeline.data('timeline-more-text')

      if ($times.length > parseInt($showNumber, 10)) {
        $timeline.append('<button tabindex="0" class="timeline__toggle"></button>')
        $times.eq(parseInt($showNumber, 10) - 1).addClass('timeline__item--fade')
        $times.slice(parseInt($showNumber, 10)).hide().addClass('timeline__item--more')
        $timeline.find('.timeline__toggle').addClass('more-times')
        $timeline.find('.timeline__toggle').html(`${$showText}<i class="icon-chevron-down"></i>`)
        // this.setUpToggle($timeline.find('.timeline__toggle'))
      }
    }
  }

  setUpToggle(element) {
    $(element).click((evt) => {
      evt.preventDefault()

      const $toggleButton = $(evt.currentTarget)
      const $toggleParent = $toggleButton.parent()
      const $lessText = $toggleParent.data('timeline-less-text')
      const $moreText = $toggleParent.data('timeline-more-text')

      if ($toggleButton.hasClass('more-times')) {
        $toggleParent.addClass('timeline__items--expanded')
        $toggleParent.find('.timeline__item--more').show()
        $toggleParent.find('.timeline__item.timeline__item--more:eq(0) .timeline__link').focus()
        $toggleButton.removeClass('more-times')
        $toggleButton.html(`${$lessText}<i class="icon-chevron-up"></i>`)
      } else {
        $toggleParent.children('.timeline__item--more').hide()
        $toggleButton.html(`${$moreText}<i class="icon-chevron-down"></i>`)
        $toggleParent.removeClass('timeline__items--expanded')
        $toggleButton.addClass('more-times')

        $('html, body').animate({
          scrollTop: $toggleButton.closest('.tabs').offset().top},
          500,
          () => {
            $toggleParent.find('.timeline__item:not(.timeline__item--more):eq(-1) .timeline__link').focus()
          }
        )
      }
    }).keydown((evt) => {
      const keyCode = window.event ? evt.which : evt.keyCode
      if (keyCode === 13) {
        $(evt.currentTarget).click()
        return false
      }
    })
  }
}

const timeline = new Timeline()


/** Form **/

class Form {

  constructor(formID) {
    this.config = {
      formID,
    }

    this.handleSubmisssion(this.config.formID)
    this.setUpMessage()
  }

  setUpMessage() {
    $('#subscription-message').click((evt) => {
      evt.preventDefault()
      evt.stopPropagation()
      $(evt.currentTarget).hide()
    }).keydown((evt) => {
      const keyCode = window.event ? evt.which : evt.keyCode
      if (keyCode === 13) {
        $(evt.currentTarget).click()
        return false
      }
    })
  }

  validateInput() {
    return (_, input) => {
      const $input = $(input)
      if ($input.val() === '') {
        $input.parent().addClass('has-error')
        $input.parent().next('.error_msg').show()
      } else if ($input.attr('id') === 'q324541_q3') {
        if(this.validateEmail($(`#${$input.attr('id')}`).val())){
          $input.parent().removeClass('has-error')
          $input.parent().next('.error_msg').hide()
        } else {
          $input.parent().addClass('has-error')
          $input.parent().next('.error_msg').show()
        }
      } else {
        $input.parent().removeClass('has-error')
      }
    }
  }

  validateEmail(input) {
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    return (input.match(mailformat)) ? true : false
  }

  handleSubmisssion(formID) {
    const $form = $(`#form_email_${formID}`)
    const $submitButton = $(`#form_email_${formID}_submit`)

    $form.submit(() => {
      $.each($form.find('input[type="text"]'), this.validateInput())

      if (!$('.has-error').length) {
        $submitButton.attr('disabled', true).find('i').css('display', 'inline-block')

        $.ajax({
          type: $form.attr('method'),
          url: $form.attr('action'),
          data: `${$form.serialize()}&form_email_${formID}_submit=Submit`,
          success: () => {
            $submitButton.attr('disabled', false).find('i').hide()
            $form.trigger('reset')
            $('#subscription-message').show()
          },
          error: () => {
            $submitButton.attr('disabled', false).find('i').hide()
          },
        })
      }

      return false
    })
  }
}

const subscriptionForm = new Form('324541')

class BackToTop {

  constructor(selector, config) {
    this.attach = this.attach.bind(this)
    this.detach = this.detach.bind(this)

    this.config = {
      selectors: {
        footer: 'footer',
        main: '#main__content',
        sideNav: '.inpage__navigation',
      },
      width: '960px',
      clearance: 120,
    }

    this.$selector = $(selector)
    $.extend(this.config, config)

    this.$window = $(window)
    this.height = this.$window.height()

    if (window.matchMedia) {
      this.mql = window.matchMedia(`screen and (min-width: ${this.config.width})`)
    }

    if (this.$selector.length) {
      this.testContentHeight()
      this.$selector.bind('click', (evt) => { evt.preventDefault() })
      this.$window.on('scroll', this.reposition.bind(this))
    }
  }

  reposition() {
    const top = this.$window.scrollTop()
    const navBottom = ($(this.config.selectors.sideNav).offset().top + this.config.clearance)
    const bottom = (this.$footer.offset().top - this.height) + 20
    let trigger = 0

    if (window.matchMedia) {
      if (!this.mql.matches) {
        trigger = this.height * 0.5
      } else {
        trigger = parseInt((navBottom), 10)
      }
    }

    if (top > trigger) {
      const action = (top > bottom) ? this.attach : this.detach
      action()
      this.show()
    } else {
      this.hide()
    }
  }

  attach() {
    return this.$selector.attr('data-stick', true)
  }

  detach() {
    return this.$selector.attr('data-stick', false)
  }

  show() {
    return this.$selector.attr('data-show', true)
  }

  hide() {
    return this.$selector.attr('data-show', false)
  }

  testContentHeight() {
    this.$footer = $(this.config.selectors.footer)

    if (this.height > this.$footer.offset().top) {
      return this.$selector.remove()
    }
  }
}


const backToTop = new BackToTop('.button--top')
