import gulp from 'gulp'
import runSequence from 'run-sequence'
import del from 'del'
import autoprefixer from 'autoprefixer'
import pixrem from 'pixrem'
import mmq from 'gulp-merge-media-queries'
import pug from 'gulp-pug'
import browserSync from 'browser-sync'
import imagemin from 'gulp-imagemin'
import concat from 'gulp-concat'
import uglify from 'gulp-uglify'
import sourcemaps from 'gulp-sourcemaps'
import sass from 'gulp-sass'
import postcss from 'gulp-postcss'
import notify from 'gulp-notify'
import replace from 'gulp-replace'
import stripCssComments from 'gulp-strip-css-comments'
import htmlbeautify from 'gulp-html-beautify'
import a11y from 'gulp-a11y'
import babel from 'gulp-babel'
import cssnano from 'gulp-cssnano'

/*
 * Clean Dist Folder
 */
gulp.task('clean', () => del(['dist']))


/*
 * Copy image to ./dist and optimise
 */
gulp.task('images', () => {
  gulp.src('src/assets/images/**/*')
  .pipe(imagemin({
    progressive: true,
    interlaced: true,
  }))
  .pipe(gulp.dest('dist/assets/images'))
  .pipe(browserSync.reload({ stream: true }))
})


/*
 * Copy font files to ./dist
 */
gulp.task('fonts', () => {
  gulp.src('src/assets/fonts/**')
  .pipe(gulp.dest('dist/assets/fonts'))
})


/*
 * Move Main JS file to Dist
 */
gulp.task('js:main', () => {
  gulp.src([
    'src/assets/js/**/*.js',
    '!src/assets/js/vendor/*.js',
  ])
  .pipe(babel({
    presets: ['es2015'],
  }))
  .on('error', notify.onError({
    message: 'Error: <%= error.message %>',
  }))
  .pipe(uglify())
  .pipe(gulp.dest('dist/assets/js'))
  .pipe(browserSync.reload({ stream: true }))
})


/*
 * Concat all vendor JS files
 */
gulp.task('js:vendor', () => {
  gulp.src([
    'src/assets/js/vendor/jquery.js',
    'src/assets/js/vendor/*.js',
  ])
  .on('error', notify.onError({
    message: 'Error: <%= error.message %>',
  }))
  .pipe(concat('vendor.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dist/assets/js'))
})


/*
 * Task: Run all JS tasks
 */
gulp.task('js', (cb) => {
  runSequence(['js:main', 'js:vendor'], cb)
})


/*
 * Build static html files via templating engine
 */
gulp.task('html', () => {
  gulp.src('src/**/*.pug')
  .pipe(pug())
  .on('error', notify.onError({
    message: 'Error: <%= error.message %>',
  }))
  .pipe(htmlbeautify({ indentSize: 2 }))
  .pipe(gulp.dest('dist/'))
})


/*
 * Compile CSS
 */
gulp.task('css', () => {
  const processors = [
    autoprefixer({
      browsers: ['IE 8', 'IE 9', 'last 2 versions'],
    }),
    pixrem({
      replace: false,
      rootValue: 10,
      atrules: true,
    }),
  ]

  return gulp.src('src/assets/scss/*.scss')
    // .pipe(sourcemaps.init())
    .pipe(sass({
      // style: 'compressed',
      noCache: true,
    }))
    .on('error', notify.onError({
      message: 'Error: <%= error.message %>',
    }))
    // .pipe(sourcemaps.write())
    .pipe(postcss(processors))
    // .pipe(stripCssComments({ preserve: false }))
    .on('error', notify.onError({
      message: 'Error: <%= error.message %>',
    }))
    .pipe(mmq({ log: true }))
    .pipe(gulp.dest('dist/assets/css'))
    .pipe(browserSync.reload({ stream: true }))
})


/*
 * Copy files
 */
gulp.task('copy:npm', () => {
  gulp.src(['node_modules/jquery/dist/jquery.js'])
    .pipe(gulp.dest('src/assets/js/vendor'))

  gulp.src(['node_modules/jquery-match-height/dist/jquery.matchHeight.js'])
    .pipe(gulp.dest('src/assets/js/vendor'))

  // gulp.src(['node_modules/devbridge-autocomplete/dist/jquery.autocomplete.js'])
  //   .pipe(gulp.dest('src/assets/js/vendor'))

  // gulp.src(['node_modules/readmore-js/readmore.min.js'])
  //   .pipe(gulp.dest('dist/assets/js'))

  // gulp.src(['node_modules/webfontloader/webfontloader.js'])
  //   .pipe(gulp.dest('dist/assets/js'))

  gulp.src(['node_modules/normalize.css/normalize.css'])
    .pipe(gulp.dest('dist/assets/css'))

  gulp.src(['node_modules/include-media/dist/_include-media.scss'])
    .pipe(gulp.dest('src/assets/scss/mixins'))
})


/*
 * Matrix String Replace
 */
gulp.task('css-matrix', () => {
  gulp.src(['dist/assets/css/*.css'])
  .pipe(replace(/..\/images\//gi, 'mysource_files/'))
  .pipe(stripCssComments({ preserve: false }))
  // .pipe(cleanCSS())
  .pipe(cssnano())
  .pipe(gulp.dest('dist/assets/css'))
})


/*
 * Accessibilty Audit
 */
gulp.task('audit', () => {
  gulp.src('dist/*.html')
    .pipe(a11y())
    .pipe(a11y.reporter())
})


/*
 * Task: Run BrowserSync and watch files
 */
gulp.task('watch', ['default'], () => {
  browserSync.init({ server: 'dist' })
  gulp.watch(['src/assets/fonts/**/*.+(ttf|woff|eot|svg|woff2)'], ['fonts'])
  gulp.watch(['src/assets/scss/**/*.scss'], ['css'])
  gulp.watch(['src/assets/js/**/*.js'], ['js:main'])
  gulp.watch(['src/assets/js/vendor/*.js'], ['js:vendor'])
  gulp.watch(['src/assets/images/**/*'], ['images'])
  gulp.watch(['src/**/*.pug'], ['html', browserSync.reload])
})


/*
 * Task: Default
 */
gulp.task('default', ['clean'], (cb) => {
  runSequence('copy:npm', ['html', 'images', 'fonts', 'css'], 'js', cb)
})


/*
 * Task: Matrix Export
 */
gulp.task('prod', ['clean'], (cb) => {
  runSequence('copy:npm', 'css', ['css-matrix', 'html', 'images', 'fonts'], 'js', cb)
})
